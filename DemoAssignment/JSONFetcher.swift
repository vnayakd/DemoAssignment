//
//  JSONFetcher.swift
//  DemoAssignment
//
//  Created by Vinayak Deshpande on 9/4/15.
//  Copyright (c) 2015 Vinayak Deshpande. All rights reserved.
//

import UIKit


class JSONFetcher: NSObject{
    
    var storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var list:ListTableViewController = ListTableViewController()
    var jsonParser:JSONParser = JSONParser()
    func fetchJSON(urlStr:String){
        var parsedData:[(String,String,String,String)] = [(String,String,String,String)]()
        let httpMethod = "GET"
        let url = NSURL(string: urlStr)
        let urlRequest = NSMutableURLRequest(URL: url!,
            cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 15.0)
        let queue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(
            urlRequest,
            queue: queue,
            completionHandler: {(response: NSURLResponse!,
                data: NSData!,
                error: NSError!) in
               // println(data)
                if data.length > 0 && error == nil{
                  //  let json = NSString(data: data, encoding: NSASCIIStringEncoding)
                   self.jsonParser.parseJSON(data)
                  //  println(json)
                }else if data.length == 0 && error == nil{
                    println("Nothing was downloaded")
                } else if error != nil{
                    println("Error happened = \(error)")
                }
            }
        )
        //println(parsedData)
        
        list.ListData = parsedData
        refresh()
        
    }
    
    
    func refresh(){
        

    }
    
}