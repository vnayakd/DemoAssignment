//
//  DBManager.swift
//  sqliteDemo
//
//  Created by Vinayak Deshpande on 9/4/15.
//  Copyright (c) 2015 Vinayak Deshpande. All rights reserved.
//

import UIKit


class DBManager:NSObject {
    var fileName:NSString?
    var documentDir:NSString?
    var paths:NSArray?
    var arrRes:NSArray?
    var destPath:String?
    
    var db:COpaquePointer = nil
    var statement:COpaquePointer = nil
    
    init(fileName:NSString){
        super.init()
        self.paths  = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        self.documentDir =  paths!.objectAtIndex(0) as? NSString
        self.fileName = fileName
        self.dbToDocumentDirectory()
    }
    
    func dbToDocumentDirectory(){
        destPath = (paths![0]).stringByAppendingPathComponent(fileName as! String)
        
        println(destPath)
        var fileManager = NSFileManager.defaultManager()
        fileManager.removeItemAtPath(destPath!, error: nil)
        if !fileManager.fileExistsAtPath(destPath!) {
            var fromPath: String? = NSBundle.mainBundle().resourcePath?.stringByAppendingPathComponent(fileName as! String)
            println(fromPath)
            var error : NSError?
            fileManager.copyItemAtPath(fromPath!, toPath: destPath!, error: &error)
            
            //  println(1)
        }
        
    }
    
    func createDB(tblName:String){
        
        if(sqlite3_open(destPath!, &db) != SQLITE_OK){
            println("Error opening DB")
        }
       
    
        if(tblName == "test"){
        if sqlite3_exec(db, "create table if not exists \(tblName) (id integer primary key autoincrement, name text,image text, billerType text, entityType text)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("error creating table: \(errmsg)")
        }else{
          
        }
        }else{
            if sqlite3_exec(db, "create table if not exists \(tblName) (id integer, name text,image text, billerType text, entityType text)", nil, nil, nil) != SQLITE_OK {
                let errmsg = String.fromCString(sqlite3_errmsg(db))
                println("error creating table: \(errmsg)")
            }else{
                
            }
 
        }
        
    }
    
    func insertDB(tblName:String, values:[String]){
        
        var count = values.count
         let SQLITE_TRANSIENT = sqlite3_destructor_type(COpaquePointer(bitPattern: -1))
      
       // if(tblName == "test"){
        if sqlite3_prepare(db, "insert into \(tblName)(name,image,billerType, entityType) values (?,?,?,?)", -1, &statement, nil) !=  SQLITE_OK{
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("error preparing insert: \(errmsg)")
        }else{
           
        }
       
        
        
        if (sqlite3_bind_text(statement, 1, "\(values[0])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
        {
            
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("failure binding foo: \(errmsg)")
        }
        else
        {
           // println("----------------------\(values)")
        }
        
        if (sqlite3_bind_text(statement, 2, "\(values[1])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
        {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("failure binding foo: \(errmsg)")
        }else{
           
        }
        
        if (sqlite3_bind_text(statement, 3, "\(values[2])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
        {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("failure binding foo: \(errmsg)")
        }else{
           
        
        
        if (sqlite3_bind_text(statement, 4, "\(values[3])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
        {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("failure binding foo: \(errmsg)")
        }else{
                   }
        }
            
        
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("failure inserting foo: \(errmsg)")
            
        }
            
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("error finalizing prepared statement: \(errmsg)")
        }
        
        statement = nil
        // closeDB()Arr
    }
    
//    func insertFavDB(tblName:String, values:[(Int, String,String,String, String)]){
//        
//        var count = values.count
//        let SQLITE_TRANSIENT = sqlite3_destructor_type(COpaquePointer(bitPattern: -1))
//        
//        if(tblName == "test"){
//            if sqlite3_prepare(db, "insert into \(tblName)(id, name,image,billerType, entityType) values (?,?,?,?,?)", -1, &statement, nil) !=  SQLITE_OK{
//                let errmsg = String.fromCString(sqlite3_errmsg(db))
//                println("error preparing insert: \(errmsg)")
//            }else{
//                
//            }
//            
//            
//            
//            if (sqlite3_bind_int64(statement, 1, "\(values[0])")) != SQLITE_OK
//            {
//                
//                let errmsg = String.fromCString(sqlite3_errmsg(db))
//                println("failure binding foo: \(errmsg)")
//            }
//            else
//            {
//                // println("----------------------\(values)")
//            }
//            
//            if (sqlite3_bind_text(statement, 2, "\(values[1])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
//            {
//                let errmsg = String.fromCString(sqlite3_errmsg(db))
//                println("failure binding foo: \(errmsg)")
//            }else{
//                
//            }
//            
//            if (sqlite3_bind_text(statement, 3, "\(values[2])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
//            {
//                let errmsg = String.fromCString(sqlite3_errmsg(db))
//                println("failure binding foo: \(errmsg)")
//            }else{
//                
//                
//                
//                if (sqlite3_bind_text(statement, 4, "\(values[3])",-1, SQLITE_TRANSIENT)) != SQLITE_OK
//                {
//                    let errmsg = String.fromCString(sqlite3_errmsg(db))
//                    println("failure binding foo: \(errmsg)")
//                }else{
//                }
//            }
//            
//        }
//        if sqlite3_step(statement) != SQLITE_DONE {
//            let errmsg = String.fromCString(sqlite3_errmsg(db))
//            println("failure inserting foo: \(errmsg)")
//            
//        }
//        
//        
//        if sqlite3_finalize(statement) != SQLITE_OK {
//            let errmsg = String.fromCString(sqlite3_errmsg(db))
//            println("error finalizing prepared statement: \(errmsg)")
//        }
//        
//        statement = nil
//        // closeDB()Arr
//    }
    
    func retrieve(tblName:String,orderby:String)->[(Int, String,String,String, String)]{
        
        var dbData:[(Int, String,String,String, String)] = [(Int, String,String,String, String)]()
        var favData:[Int] = [Int]()
        var iterator = 0
        if sqlite3_prepare(db, "select * from \(tblName) order by \(orderby) asc",-1, &statement, nil) != SQLITE_OK{
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("error preparing select: \(errmsg)")
        }else{
        
      
        while sqlite3_step(statement) == SQLITE_ROW{
            
            let id = sqlite3_column_int64(statement, 0)
            let name = sqlite3_column_text(statement, 1)
            let image = sqlite3_column_text(statement, 2)
            let billerType = sqlite3_column_text(statement, 3)
            let entityType = sqlite3_column_text(statement, 4)
            
            if name != nil {
                let idInt = Int(id)
                let nameStr = String.fromCString(UnsafePointer<Int8>(name))
                let imageStr = String.fromCString(UnsafePointer<Int8>(image))
                let billerTypeStr = String.fromCString(UnsafePointer<Int8>(billerType))
                let entityTypeStr = String.fromCString(UnsafePointer<Int8>(entityType))
               dbData.insert((idInt,nameStr!,imageStr!,billerTypeStr!,entityTypeStr!), atIndex: iterator)
                iterator++
            } else {
                println("name not found")
            }
           
        }
            
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String.fromCString(sqlite3_errmsg(db))
            println("error finalizing prepared statement: \(errmsg)")
        }
        
        statement = nil
        
        }
        
        return dbData
    }
    
    func delRows(table:String){
        
        if(sqlite3_prepare_v2(db, "delete from \(table)", -1, &statement, nil) == SQLITE_OK){
            // Loop through the results and add them to the feeds array
            if(sqlite3_step(statement) == SQLITE_DONE) {
                // Deleted records,....
            }else{
               
            }
            sqlite3_finalize(statement);
        }
    }



    func closeDB(){
        if sqlite3_close(db) != SQLITE_OK {
            println("error closing database")
        }
        db = nil
    }
}
