//
//  FavTableViewController.swift
//  DemoAssignment
//
//  Created by Vinayak Deshpande on 9/6/15.
//  Copyright (c) 2015 Vinayak Deshpande. All rights reserved.
//

import UIKit




class FavTableViewController: UITableViewController,FavDelegate {

    
    var insertFavData:[(Int, String,String,String, String)] = [(Int, String,String,String, String)]()
      var imageFetcher:FetchImages = FetchImages()
     var dbm:DBManager?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //dbm!.createDB("favs")
        
            insertFavData = dbm!.retrieve("favs", orderby: "id")
        println(insertFavData)
        insertFavData.count
        if(insertFavData.count == 0){
            var viewNone:UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
            var label:UILabel = UILabel(frame: CGRectMake(200, 500, 100, 100))
            label.text = "Nothing to Display"
            viewNone.addSubview(label)
            self.view.addSubview(viewNone)
        }
        
       self.tableView.reloadData()
           }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
    println(insertFavData.count)
        return insertFavData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! TableCellsTableViewCell

       cell.IBOFavSaveBtn.hidden = true
        cell.IBOBillerName.text = insertFavData[indexPath.row].1 as String
        cell.IBOBillerType.text = insertFavData[indexPath.row].2 as String
        cell.IBOBillerEntity.text = insertFavData[indexPath.row].3 as String
        var url:NSURL = NSURL(string: ("https://www.payumoney.com\(insertFavData[indexPath.row].2)").stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!
        imageFetcher.downloadImage(url, handler: {image, error in
            if(image != nil){
                cell.IBOThumbNailImage!.image = image
            }else{
                cell.IBOThumbNailImage?.image = UIImage(named:"fav.png")
            }
        })

         return cell
    }


    func getDBMgr(dbMgr:DBManager, data:[(Int, String,String,String, String)]){
        dbm = dbMgr
        insertFavData = data
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        }
    }

