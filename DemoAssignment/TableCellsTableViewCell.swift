//
//  TableCellsTableViewCell.swift
//  DemoAssignment
//
//  Created by Vinayak Deshpande on 9/6/15.
//  Copyright (c) 2015 Vinayak Deshpande. All rights reserved.
//

import UIKit

class TableCellsTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var IBOThumbNailImage: UIImageView!
    
    
    @IBOutlet weak var IBOFavSaveBtn: UIButton!
    @IBOutlet weak var IBOBillerName: UILabel!
    @IBOutlet weak var IBOBillerType: UILabel!
    @IBOutlet weak var IBOBillerEntity: UILabel!
    
    var returnedFavData:[(Int, String,String,String, String)] = [(Int, String,String,String, String)]()
    var FavID:[String] = [String]()
     var selFav:[Int] = [Int]()
    var check:UIImage = UIImage(named: "check.png")!
    var checked:UIImage = UIImage(named: "checked.png")!
    
    
    @IBAction func IBAAddFav(sender: AnyObject) {
        if(contains(selFav, sender.tag)){
            if let index = find(selFav, sender.tag){
                selFav.removeAtIndex(index)
            }
        }else{
            selFav.append(sender.tag)
        }
        if (sender.imageView?!.image == checked) {
            sender.setImage(check, forState: UIControlState.Normal)
        } else {
            sender.setImage(checked, forState: UIControlState.Normal)
        }
//        FavID.append(String(returnedDBData[sender.tag].0))
//        
       
        if(selFav.count > 0){
        
        }else{
           
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        self.imageView?.contentMode = UIViewContentMode.ScaleAspectFill
        self.imageView?.frame =  CGRectMake(self.imageView!.frame.origin.x, self.imageView!.frame.origin.y, 50, 50)
    }
    
    

}
