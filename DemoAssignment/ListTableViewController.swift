//
//  ListTableViewController.swift
//  DemoAssignment
//
//  Created by Vinayak Deshpande on 9/4/15.
//  Copyright (c) 2015 Vinayak Deshpande. All rights reserved.
//

import UIKit

protocol FavDelegate{
    func getDBMgr(dbMgr:DBManager,data:[(Int, String,String,String, String)])
}


class ListTableViewController: UITableViewController, selectedSortOptionDelegate, UIScrollViewDelegate {
  
    
   

    var delegate:FavDelegate?
    var dbMgr:DBManager?
    @IBOutlet var table: UITableView!
    var viewBg:UIView?
    var act:UIActivityIndicatorView?
    var returnedDBData:[(Int, String,String,String, String)] = [(Int, String,String,String, String)]()
    var returnedFavData:[(Int, String,String,String, String)] = [(Int, String,String,String, String)]()
     var insertFavData:[(Int, String,String,String, String)] = [(Int, String,String,String, String)]()
    
    var ListData:[(String,String,String,String)] = [(String,String,String,String)]()
    var parse:[(String,String,String,String)] = [(String,String,String,String)]()
    var parseData = (String(),String(),String(),String())
   
    var FavID:[String] = [String]()
    var Btn:UIButton?
     var saveBtn:UIButton?
    var cell2:Int?
    var check:UIImage = UIImage(named: "check.png")!
     var Btns:UIButton?
    var checked:UIImage = UIImage(named: "checked.png")!
    var optionLabel:UILabel?
    var currentPage = 1
    var image:[UIImage] = []
    var isPageRefresing:Bool = false
    var imageCount = 0
    var imageFetcher:FetchImages = FetchImages()
    var selFav:[Int] = [Int]()
     var favIds:[Int] = [Int]()
    var eleIds:[Int] = [Int]()
    
    
    func IBASaveFavs(sender: UIButton) {
        
        println("************\(selFav)")
        
        returnedFavData = dbMgr!.retrieve("favs",orderby: "id")
                for(var iter2 = 0; iter2 < returnedFavData.count; iter2++){
             favIds.insert(returnedFavData[iter2].0, atIndex: iter2)
        }
        
                for(var iterator = 0; iterator < selFav.count; iterator++){
                    if(favIds.count>0){
                    if(contains(selFav, favIds[iterator])){
                        if let index = find(selFav,favIds[iterator]){
                            selFav.removeAtIndex(index)
                        }
                        }
                    }else{
                        Btn?.hidden = false
                       
                            if(contains(eleIds, selFav[iterator])){
                                dbMgr!.insertDB("favs", values: [returnedDBData[selFav[iterator]].1,returnedDBData[selFav[iterator]].2,returnedDBData[selFav[iterator]].3,returnedDBData[selFav[iterator]].4])
                        }
         
                        
                    }
        }
        
        }
    



    override func viewDidLoad() {
        super.viewDidLoad()
        dbMgr = DBManager(fileName: "db.sqlite")
        dbMgr?.createDB("test")
        dbMgr?.delRows("test")
        
        dbMgr?.createDB("favs")
        
        
        viewBg = UIView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.height))
        viewBg!.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(viewBg!)
        
        act = UIActivityIndicatorView(frame: CGRectMake(self.view.frame.width/2.0-10, self.view.frame.height/2.0-70, 40, 40))
        act!.startAnimating()
        act!.tintColor  = UIColor.blackColor()
        act!.backgroundColor = UIColor.grayColor()
        act!.hidesWhenStopped = true
        self.view.addSubview(act!)
        

      
        
        fetchJSON(currentPage)
        
        Btns =  UIButton(frame: CGRectMake(260, 27, 30, 30))
        Btns!.setImage(UIImage(named: "fav.png"), forState: UIControlState.Normal)
        Btns!.addTarget(self, action: Selector("fav:"), forControlEvents: UIControlEvents.TouchUpInside)
        Btns!.hidden = false
        returnedFavData = dbMgr!.retrieve("favs", orderby: "id")
        if(returnedFavData.count>0){
            Btns?.hidden = false
        }
        
        saveBtn =  UIButton(frame: CGRectMake(320, 27, 30, 30))
        saveBtn!.setImage(UIImage(named: "save.png"), forState: UIControlState.Normal)
        saveBtn!.addTarget(self, action: Selector("IBASaveFavs:"), forControlEvents: UIControlEvents.TouchUpInside)
        saveBtn!.hidden = true
        
        
        
        optionLabel = UILabel(frame: CGRectMake(50, 20, 100, 40))
        optionLabel!.text = "None"
         self.navigationController!.view.addSubview(optionLabel!)
        self.navigationController!.view.addSubview(Btns!)
        self.navigationController!.view.addSubview(saveBtn!)
    }
    
    override func viewWillAppear(animated: Bool) {
        optionLabel?.hidden = false
        Btns?.hidden = false
       
        saveBtn?.hidden = false
        if(optionLabel?.text == "Name"){
            returnedDBData = dbMgr!.retrieve("test", orderby: (optionLabel!.text!).lowercaseString)
            self.refresh()
        }
        else if(optionLabel?.text == "None"){
            returnedDBData = dbMgr!.retrieve("test", orderby: "id")
            self.refresh()
        }

        else if(optionLabel?.text == "Biller Type"){
            returnedDBData = dbMgr!.retrieve("test", orderby: "billerType")
            self.refresh()
        }

        else if(optionLabel?.text == "Entity Type"){
            returnedDBData = dbMgr!.retrieve("test", orderby: "entityType")
            self.refresh()
        }

    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(ListData.count > 0){
            viewBg?.hidden = true
            act!.stopAnimating()
        }
        return ListData.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier") as! TableCellsTableViewCell
        
        
        cell.IBOBillerName.text = returnedDBData[indexPath.row].1 as String
        
        var url:NSURL = NSURL(string: ("https://www.payumoney.com\(ListData[indexPath.row].1)").stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!
            imageFetcher.downloadImage(url, handler: {image, error in
                if(image != nil){
            cell.IBOThumbNailImage!.image = image
                }else{
                    cell.IBOThumbNailImage?.image = UIImage(named:"fav.png")
                }
        })
        

        Btn = cell.IBOFavSaveBtn
        cell.IBOFavSaveBtn.tag = returnedDBData[indexPath.row].0
        cell.IBOFavSaveBtn.frame.size = CGSize(width: 20, height: 20)
        cell.IBOFavSaveBtn.setImage(checked, forState: UIControlState.Normal)
        cell.IBOFavSaveBtn.addTarget(self, action: Selector("save:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.IBOBillerEntity.text = returnedDBData[indexPath.row].4 as String
        cell.IBOBillerType.text = returnedDBData[indexPath.row].3 as String
        
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
    }
    
   func save(sender:UIButton){
        if(contains(selFav, sender.tag)){
            if let index = find(selFav, sender.tag){
                selFav.removeAtIndex(index)
            }
        }else{
            selFav.append(sender.tag)
        }
        if (sender.imageView?.image == checked) {
            sender.setImage(check, forState: UIControlState.Normal)
        } else {
            sender.setImage(checked, forState: UIControlState.Normal)
        }
        //FavID.append(String(returnedDBData[sender.tag].0))
        println(selFav)
        
        if(selFav.count > 0){
            saveBtn?.hidden = false
        }else{
             saveBtn?.hidden = true
        }

    }
    
    
    
    func fetchJSON(pageNumber:Int){
          var urlStr:String = "https://www.payumoney.com/WebService/app/webService/findBiller?count=20&offset=\(pageNumber)"
        var parsedData:[(String,String,String,String)] = [(String,String,String,String)]()
        let httpMethod = "GET"
        let url = NSURL(string: urlStr)
        let urlRequest = NSMutableURLRequest(URL: url!,
            cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 15.0)
        let queue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(
            urlRequest,
            queue: queue,
            completionHandler: {(response: NSURLResponse!,
                data: NSData!,
                error: NSError!) in
                // println(data)
                if data == nil && error == nil{
                    //  let json = NSString(data: data, encoding: NSASCIIStringEncoding)
                    var alert:UIAlertView = UIAlertView(title: "Data Not downloaded", message: "Please check your internet connection", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    //  println(json)
                }else if data != nil && error == nil{
                   self.parseJSON(data)
                } else if error != nil{
                    var alert:UIAlertView = UIAlertView(title: "Data Not downloaded", message: "Request Time out", delegate: nil, cancelButtonTitle: "OK")
                    self.act?.stopAnimating()
                    alert.show()
                    
                }
            }
        )
    }
    
    func  fav(sender:UIButton){

        var storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc:FavTableViewController = storyBoard.instantiateViewControllerWithIdentifier("favTable") as! FavTableViewController
         self.delegate = vc
        if(self.delegate != nil) {
            self.delegate?.getDBMgr(dbMgr!,data: dbMgr!.retrieve("favs", orderby: "id"))
        }
        self.navigationController?.pushViewController(vc, animated: true)
       
        
        optionLabel?.hidden = true
        Btns?.hidden = true
        saveBtn?.hidden = true
    }
    
    func fetchImages(url:String){
        println(imageCount)
        var urlStr:String = url
        let httpMethod = "GET"
        let url = NSURL(string: urlStr)
        if(url != " " && url != nil){
            println("\(url)")
        let urlRequest = NSMutableURLRequest(URL: url!,
            cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 25.0)
        let queue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(
            urlRequest,
            queue: queue,
            completionHandler: {(response: NSURLResponse!,
                data: NSData!,
                error: NSError!) in
                if data == nil && error == nil{
                    var alert:UIAlertView = UIAlertView(title: "Data Not downloaded", message: "Please check your internet connection", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }else if data != nil && error == nil{
                    self.image.insert((UIImage(data: data)!), atIndex: self.imageCount)
                    self.imageCount++

                    
                } else if error != nil{
                    var alert:UIAlertView = UIAlertView(title: "Data Not downloaded", message: "Request Time out", delegate: nil, cancelButtonTitle: "OK")
                    self.act?.stopAnimating()
                    alert.show()
                    
                    
                }
            }
        )
        }else{
            self.image.insert((UIImage(named: "fav.png")!), atIndex: imageCount)
            
            self.imageCount++
        }

    }
    
    func parseJSON(data:NSData){
        var parseError: NSError?
        var iterator:Int = Int()
        let json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError)
        if (parseError == nil)
        {
            var null:NSNull = NSNull()
            var result: NSMutableDictionary? = json?.valueForKey("result") as? NSMutableDictionary
            var dataList:NSMutableArray = (result?.valueForKey("dataList") as? NSMutableArray)!
            while(iterator < dataList.count){
                
                if(dataList.objectAtIndex(iterator).valueForKey("name")! as! NSObject == null){
                    parseData.0 = " "
                }else {
                    parseData.0 = (dataList.objectAtIndex(iterator).valueForKey("name")! as? String)!
                }
                if(dataList.objectAtIndex(iterator).valueForKey("logo")! as! NSObject == null){
                    parseData.1 = " "
                }else {
                    parseData.1 = (dataList.objectAtIndex(iterator).valueForKey("logo")! as? String)!
                }
                if(dataList.objectAtIndex(iterator).valueForKey("billerType")! as! NSObject == null){
                    parseData.2 = " "
                }else {
                    parseData.2 = (dataList.objectAtIndex(iterator).valueForKey("billerType")! as? String)!
                }
                if(dataList.objectAtIndex(iterator).valueForKey("entityType")! as! NSObject == null){
                    parseData.3 = " "
                }else {
                    parseData.3 = (dataList.objectAtIndex(iterator).valueForKey("entityType")! as?
                        String)!
                    
                    
                    
                }
                parse.insert(parseData, atIndex: iterator)
                iterator++
                
            }
            ListData = parse
            
            for(var iterator = 0;iterator < ListData.count; iterator++){
                dbMgr?.insertDB("test", values: [ListData[iterator].0,ListData[iterator].1,ListData[iterator].2,ListData[iterator].3])
                
                
                
            }
            returnedDBData = dbMgr!.retrieve("test", orderby: "id")
            println(returnedDBData)
            
            for(var iterator = 0; iterator < returnedDBData.count; iterator++){
                println("-----------------\(returnedDBData[iterator].0)")
                eleIds.insert(returnedDBData[iterator].0, atIndex: iterator)
            }
           
            
            

            
        }
        
        self.refresh()
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)) {
            
            //NSLog(@" scroll to bottom!");
            if(isPageRefresing ==  false){ // no need to worry about threads because this is always on main thread.
                
                isPageRefresing = true;
                
                currentPage = currentPage + 1;
                
                fetchJSON(currentPage)
                
            }
        }
    }
    
    
    
   
    

    
    
    
    
    func selectedOption(selectedSortOptionText: String) {
        optionLabel!.text = selectedSortOptionText as String
    }
    
    
    func refresh(){
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
            return
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "selectOption") {
            // pass data to next view
            var sortTableVC:SortTableViewController = segue.destinationViewController as! SortTableViewController
            sortTableVC.delegate = self
        }
        if (segue.identifier == "toFav"){
            var vc:FavTableViewController = segue.destinationViewController as! FavTableViewController
            vc.dbm = dbMgr
            
        }

    }
    
    
    
}
