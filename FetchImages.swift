
//
//  fetchImages.swift
//  DemoAssignment
//
//  Created by Vinayak Deshpande on 9/6/15.
//  Copyright (c) 2015 Vinayak Deshpande. All rights reserved.
//

import UIKit


class FetchImages: NSObject {
    
    var imageCache = [String:UIImage]()
    var fm:NSFileManager = NSFileManager.defaultManager()
    var cacheDirPath:String?
    var files:String?
    
    func getPath(file:String){
        var paths  = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        var cacheDirPath = paths[0] as! NSString
       files = cacheDirPath.stringByAppendingPathComponent(file)
        
    }
    
    
  func downloadImage(url: NSURL, handler: ((image: UIImage?, NSError!) -> Void))
    {
        
        
        let httpMethod = "GET"
        //let url = NSURL(string: urlStr)
         var imageRequest: NSURLRequest = NSMutableURLRequest(URL: url,
            cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 15.0)
        var file:String = url.absoluteString!
        getPath((file.lastPathComponent).stringByDeletingLastPathComponent)
        
        
        
        NSURLConnection.sendAsynchronousRequest(imageRequest,
            queue: NSOperationQueue.mainQueue(),
            completionHandler:{response, data, error in
                if(data != nil){
                handler(image: UIImage(data: data)!, error)
                  self.fm.createFileAtPath(self.files!, contents: data, attributes: nil)
                println(self.files)
                }
        })
        
    }
}